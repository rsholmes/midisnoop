# midisnoop.kicad_sch BOM

Wed 10 Jan 2024 06:02:07 PM EST

Generated from schematic by Eeschema 7.0.10-7.0.10~ubuntu22.04.1

**Component Count:** 10


## 
| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| A1 | 1 | Arduino_Nano_v3.x | Arduino Nano v3.x |
| C3 | 1 | 0.1uF | Unpolarized capacitor |
| D1 | 1 | LED | Light emitting diode |
| D4 | 1 | 1N4148 | 100V 0.15A standard switching diode, DO-35 |
| J2 | 1 | DIN-5_180degree | 5-pin DIN connector (5-pin DIN-5 stereo) |
| R1 | 1 | R | Resistor |
| R2 | 1 | 220R | Resistor |
| R3 | 1 | 3.9k | Resistor |
| U1 | 1 | MC096GX | 128*64 monochrome I2C OLED module |
| U2 | 1 | 6N137 | Single High Speed LSTTL/TTL Compatible Optocoupler with enable, dV/dt 1000/us, VCM 10, max 7V VCC, DIP-8 |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|220R|1|R2|
|3.9k|1|R3|
|R|1|R1|

